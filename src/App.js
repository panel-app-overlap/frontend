import React from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Error from "./pages/Error";
import Home from "./pages/Home";

const App = () => {
	return (
		<Router>
			<Switch>
				<Route exact path="/">
					<Home />
				</Route>
				<Route path="*">
					<Error code={404} />
				</Route>
			</Switch>
		</Router>
	);
};

export default App;
