const colors = require("tailwindcss/colors");

module.exports = {
	purge: ["./src/**/*.{js,jsx,ts,tsx}", "./public/index.html"],
	darkMode: false,
	theme: {
		extend: {
			colors: {
				gray: colors.trueGray,
				sky: colors.sky,
				orange: colors.orange,
				indigo: colors.indigo,
				green: colors.green,
				rose: colors.rose,
				yellow: colors.yellow
			},
			boxShadow: {
				dark: "-2px 2px 13px -1px rgba(0,0,0,0.75)"
			},
			minHeight: {
				screen: "calc(100vh - 4rem)"
			},
			maxWidth: {
				"8xl": "96rem"
			}
		}
	},
	variants: {
		extend: {
			display: ["group-hover"],
			padding: ["last"],
			margin: ["last"],
			backgroundColor: ["disabled", "odd", "even"],
			cursor: ["disabled", "hover"],
			borderWidth: ["last"]
		}
	},
	plugins: []
};
